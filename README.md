# rmq-producer-consumer

## Test project for RQM task consume/produce

### Example RMQ message (publish into example_consume_queue)
```
{
    "uuid": "EXRMQMSG::c18bad1e-6476-11ec-b5ca-02420a001428",
    "ts": "2022-02-11 08:02:55.771811",
    "publisher": "EXRMQM",
    "created_at": "2022-02-11 08:02:55.771811",
    "command": "example_command"
}
```

### Example RMQ Loopback message (consume, then publish)
```
{
    "uuid": "EXRMQMSG::c18bad1e-6476-11ec-b5ca-02420a001428",
    "ts": "2022-02-11 08:02:55.771811",
    "publisher": "EXRMQM",
    "created_at": "2022-02-11 08:02:55.771811",
    "command": "loopback_message"
}
```

### Scheduling

In another "main" file schedule.py - an example how to user RMQ as scheduled queue through:
1) periodic background task
2) scheduled consumer
3) special producers for another microservices

### RMQ Admin
http://localhost:15672/