log:
	docker logs rmq_example -f

build:
	docker-compose build

up:
	docker-compose up -d

d:
	docker-compose down

r:
	make build
	make up

rl:
	make build
	make up
	make log
