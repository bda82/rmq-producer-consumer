aiorabbit==1.0.0a1
aiohttp==3.7.4.post0
async-timeout==3.0.1
uvloop>=0.16.0
marshmallow==3.13.0
PyYAML==5.4.1
pamqp~=3.1.0