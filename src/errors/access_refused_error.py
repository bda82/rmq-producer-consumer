from .base_error import BaseError


class AccessRefusedError(BaseError):
    
    def __init__(self, message: str = ''):
        super().__init__(message, __file__)