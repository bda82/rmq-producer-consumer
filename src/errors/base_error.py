import re
from logger import create_logger


class BaseError(Exception):
    
    def __init__(self, message: str = '', file: str = None) -> None:
        super().__init__()
        self.name = self.__class__.__name__
        self.message = message if message else ' '.join(re.findall('[A-Z][^A-Z]*', self.name)).capitalize()
        self.file = file if file else __file__
        self.status = 402
        self.logger = create_logger(self.file, False).getChild(self.name)
        
    def __str__(self) -> str:
        return self.message
    
    def __repr__(self) -> str:
        return self.message
    
    def json(self):
        return {'error': self.name,
                'message': self.message,
                'status': self.status}
       
