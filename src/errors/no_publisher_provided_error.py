from .base_error import BaseError


class NoPublisherProvidedError(BaseError):

    def __init__(self, message: str = ''):
        super().__init__(message, __file__)
