import uuid


def generate_message_uuid():
    return uuid.uuid1()