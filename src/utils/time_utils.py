import datetime

from config import Config

use_local_time = Config().common.use_local_time


def current_datetime():
    if use_local_time:
        return datetime.datetime.now().replace(microsecond=0)
    return datetime.datetime.utcnow().replace(microsecond=0)


def current_timestamp():
    if use_local_time:
        return int(datetime.datetime.now().replace(microsecond=0).timestamp())
    return int(datetime.datetime.utcnow().replace(microsecond=0).timestamp())


def current_timestring():
    return str(current_datetime())
