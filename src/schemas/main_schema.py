from marshmallow import Schema, fields


class MainSchema(Schema):
    publisher = fields.String(required=True)
    uuid = fields.String(required=True)
    created = fields.String(required=True)
    started = fields.String(required=True, allow_none=True)
    completed = fields.String(required=True, allow_none=True)
    command = fields.String(required=True)
    data = fields.Dict(required=False, allow_none=True)

    class Meta:
        unknown = "EXCLUDE"


main_schema = MainSchema()
