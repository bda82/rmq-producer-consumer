import uvloop
import asyncio

from logger import create_logger

from consumers import *

uvloop.install()

logger = create_logger(__file__)


async def main():
    logger.info('Example RQM Consumer/Producer started...')
    try:
        await MainConsumer().consume()
    except KeyboardInterrupt:
        logger.info('MainConsumer keyboard interrupt...')


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass

