import asyncio
import uvloop
from aiohttp import web

from consumers import *
from tasks import *

uvloop.install()


async def on_startup(app: web.Application):
    running_loop = asyncio.get_running_loop()

    running_loop.create_task(start_schedule_consumer())
    running_loop.create_task(ScheduleTask().start())


def init_app():
    app = web.Application()
    app.on_startup.append(on_startup)
    return app


def main():
    app = init_app()
    web.run_app(app)


if __name__ == '__main__':
    main()
