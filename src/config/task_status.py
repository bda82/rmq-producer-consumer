

from curses import ERR


class TaskStatus:
    OK = 'OK'
    ERROR = 'ERROR'
    NULL = 'NULL'
    TASK_STATUS_LIST = [
        OK,
        ERROR,
        NULL
    ]
