import os
from aiorabbit.__version__ import version


class RabbitConfig:
    
    def __init__(self) -> None:
        self.host = os.environ.get('RABBITMQ_HOST', 'localhost')
        self.port = os.environ.get('RABBITMQ_PORT', 5672)
        self.user = os.environ.get('RABBITMQ_DEFAULT_USER', 'root')
        self.password = os.environ.get('RABBITMQ_DEFAULT_PASS', 'password')
        self.virtual_host = os.environ.get('RABBITMQ_DEFAULT_VIRTUAL_HOST', '/')
        self.delivery_mode = os.environ.get('RABBITMQ_DEFAULT_DELIVERY_MODE', 2)
        self.prefetch_count = os.environ.get('RABBITMQ_DEFAULT_PREFETCH_COUNT', 0)
        self.rabbitmq_url = f'amq://{self.user}:{self.password}@{self.host}:{self.port}/%2f?heartbeat=300'
        self.default_locale = 'ru-RU'
        self.default_product = 'aiorabbit/{}'.format(version)
        self.name_separator = ':::'
