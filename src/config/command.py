

class Command:
    example_command = 'example_command'
    loopback_message = 'loopback_message'
    example_schedule_command = 'example_schedule_command'
    COMMAND_LIST = [
        example_command,
        loopback_message,
        example_schedule_command,
    ]
