from .queue import Queue
from .command import Command
from .prefix import Prefix
from .rabbit_config import RabbitConfig
from .timings import Timings
from .common import Common
from .task_status import TaskStatus
from .publishers import Publishers
from .messages import Messages


class Config:
    common = Common()
    queues = Queue()
    prefixes = Prefix()
    commands = Command()
    rabbit = RabbitConfig()
    timings = Timings()
    task_status = TaskStatus()
    publishers = Publishers()
    messages = Messages()
