

class Prefix:
    example_prefix = 'EXRMQ'
    schedule_prefix = 'SCHED'
    PREFIX_LIST = [
        example_prefix,
        schedule_prefix,
    ]
