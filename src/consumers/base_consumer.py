from abc import ABC
import json
import asyncio

from aiorabbit import client as aio_client
from aiorabbit import exceptions as aio_exceptions
from aiorabbit import message as aio_message

from pamqp import heartbeat

from config import Config as ConsumerConfig
from errors import *
from utils import *
from logger import create_logger


class BaseConsumer(ABC):

    def __init__(self, file: str = __file__, logger=None) -> None:
        self.name = self.__class__.__name__
        self.file = file if not file else __file__
        self.logger = logger or create_logger(self.file, False).getChild(self.name)
        self.queue = None
        self.shutdown = asyncio.Event()
        self.config = ConsumerConfig()
        self.client = aio_client.Client(self.config.rabbit.rabbitmq_url)
        self.channel = None
        self.function = None
        
    async def consume(self) -> None:
        """Start RMQ consuming"""
        if self.queue is None:
            raise NoQueueProvidedError(f'"queue" parameter not provided in {self.name}')
        
        if self.function is None:
            raise NoFunctionProvidedError(f'"function" parameter not provided in {self.name}')

        while True:
            try:
                await self.client.connect()
                break
            except TypeError:
                self.logger.warning(f'RMQ try reconnect to URL {self.config.rabbit.rabbitmq_url}')
                await asyncio.sleep(self.config.timings.rabbit_try_connect_time)
            except aio_exceptions.AccessRefused as e:
                self.logger.error(f'RQM access refused: {e}')
                raise AccessRefusedError(f'RQM access refused: {e}')
            except Exception as e:
                self.logger.error(f'RQM exception: {e}')
                raise

        await self.client.queue_declare(queue=self.queue)

        tag = await self.client.basic_consume(queue=self.queue,
                                              callback=self.on_message)

        self.logger.info(f'Start {self.name} consumer with queue {self.queue} at {current_timestring()}')

        while not self.client.is_closed:
            self.client._channel0.process(heartbeat.Heartbeat())
            await asyncio.sleep(self.config.timings.rabbit_heartbeat_time)

        await self.shutdown.wait()
        self.logger.info(f'Shutting down {self.name} consumer with queue {self.queue} at {current_timestring()}')
        
        await self.client.basic_cancel(tag)
        await self.client.close()
        self.logger.info(f'Exiting {self.name} consumer with queue {self.queue} at {current_timestring()}')
    
    def update_task_status(self, task_status):
        """Just for future use"""
        if self.config.common.log_all_messages:
            self.logger.debug(f'RMQ task status updated: {task_status}')
    
    async def on_message(self, message: aio_message.Message):
        """Process RMQ on_message event"""
        try: 
            await self.client.basic_ack(message.delivery_tag)

            try:
                payload = json.loads(message.body)
                if self.config.common.log_all_messages:
                    self.logger.info(f'RMQ Get message: {payload}')
            except Exception as e:
                self.logger.error(f'RMQ message JSON decode error: {e}')
                return

            command = payload.get('command', None)
            if command is None:
                self.logger.error(f'RMQ no command provided in payload {payload}')
                return

            publisher = payload.get('publisher') or 'UNKNOWN_PUBLISHER'
            publisher_uuid = payload.get('uuid') or 'UNKNOWN_UUID'
            created = payload.get('created') or current_timestring()
            started = current_timestring()
            data = payload.get('data')

            task_data = {'publisher': publisher,
                         'uuid': publisher_uuid,
                         'created': created,
                         'started': started,
                         'completed': None,
                         'command': command,
                         'data': data}

            try:
                func = self.function()

                await func.run(command, task_data)

                response = func.response

                if response.get('error'):
                    task_data['status'] = self.config.task_status.ERROR
                else:
                    task_data['completed'] = current_timestring()
                    task_data['status'] = self.config.task_status.OK

            except Exception as e:
                self.logger.error(f'RMQ function execution error: {e}')
                raise

            self.update_task_status(task_status=task_data)

        except Exception as e:
            self.logger.critical(f'RMQ critical error: {e}')
