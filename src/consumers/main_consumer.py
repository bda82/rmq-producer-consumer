
from venv import create
from logger import create_logger

from consumers import BaseConsumer

from functions import MainFunction

from config import Config


consumer_logger = create_logger(__file__, False)


class MainConsumer(BaseConsumer):
    
    def __init__(self) -> None:
        super().__init__(__file__, consumer_logger)
        self.queue = Config().queues.example_consume_queue
        self.function = MainFunction
