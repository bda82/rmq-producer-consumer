import json
from logger import create_logger
from aiorabbit import message as aio_message

from consumers import BaseConsumer

from utils import *

from config import Config as ScheduleConfig

consumer_logger = create_logger(__file__, False)


class ScheduleConsumer(BaseConsumer):

    def __init__(self) -> None:
        super().__init__(__file__, consumer_logger)
        self.config = ScheduleConfig()
        self.queue = self.config.queues.schedule_queue
        self.function = None

    async def on_message(self, message: aio_message.Message):
        """
        This method should be overwrite from BaseConsumer

        CustomProducer should publish message for another Microservice
        which is needed to run by schedule with according parameters:
        queue, command, prefix, etc.
        """
        tag = message.delivery_tag
        try:
            commands = [
                self.config.commands.example_schedule_command,
            ]
            for command in commands:
                # await CustomProducer().publish(command=command, data=None)
                self.logger.info(f'Publish schedule command: {command}')
        except Exception as e:
            self.logger.error(f'Schedule publish error: {e}')
            raise
        finally:
            await self.client.basic_ack(tag)


async def start_schedule_consumer():
    try:
        await ScheduleConsumer().consume()
    except KeyboardInterrupt:
        pass
