import os
from pathlib import Path
import logging
import logging.config
from typing import Optional, List

BASE_DIR = Path(__file__).parent.parent


BASE_DIR = BASE_DIR
LOGS_DIR = 'logs'
DEFAULT_LOGGING_LEVEL: int = logging.DEBUG
DEFAULT_FILE_LOGGING_LEVEL: int = logging.INFO
DEFAULT_DATE_FORMAT: str = '%Y-%m-%d %H:%M:%S'
DEFAULT_STDOUT_FORMAT: str = '[%(asctime)s] - %(levelname)-8s: %(name)-32s: %(funcName)-32s: %(message)s'
DEFAULT_FILE_FORMAT: str = '%(asctime)s;%(levelname)-8s;%(name)-40s;%(funcName)-30s;%(message)s'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': DEFAULT_STDOUT_FORMAT,
            'datefmt': DEFAULT_DATE_FORMAT,
        },
    },
    'handlers': {
        'stdout': {
            'level': DEFAULT_LOGGING_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
    },
    'loggers': {
        '': {
            'handlers': ['stdout'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'PIL': {
            'handlers': ['stdout'],
            'level': 'INFO',
            'propagate': False,
        },
        'asyncio': {
            'handlers': ['stdout'],
            'level': 'INFO',
            'propagate': False,
        },
        'aiohttp': {
            'handlers': ['stdout'],
            'level': 'WARNING',
            'propagate': False,
        },
        'pika': {
            'handlers': ['stdout'],
            'level': 'WARNING',
            'propagate': False,
        },
        'urllib3': {
            'handlers': ['stdout'],
            'level': 'WARNING',
            'propagate': False,
        },
        'aiorabbit': {
            'handlers': ['stdout'],
            'level': 'WARNING',
            'propagate': False,
        },
    }
}

logging.config.dictConfig(LOGGING)


def create_logger_name(filename: str) -> str:
    return Path(filename).stem


def getLogger(name: str,
              filename: Optional[str] = None,
              level: int = DEFAULT_LOGGING_LEVEL,
              file_level: int = DEFAULT_FILE_LOGGING_LEVEL,
              file_handler_format: str = DEFAULT_FILE_FORMAT,
              file_handler_date_format: str = DEFAULT_DATE_FORMAT):

    logger = logging.getLogger(name)
    logger.setLevel(level)

    if filename:
        if isinstance(filename, str) and '/' not in filename:
            if BASE_DIR:
                if not os.path.exists(f'{BASE_DIR}/{LOGS_DIR}'):
                    os.makedirs(f'{BASE_DIR}/{LOGS_DIR}')
                filename = f'{BASE_DIR}/{LOGS_DIR}/{filename}'

        file_handler = logging.FileHandler(filename)
        formatter = logging.Formatter(file_handler_format, file_handler_date_format)
        file_handler.setFormatter(formatter)
        file_handler.setLevel(file_level)
        logger.addHandler(file_handler)

    return logger


def create_logger(path_file: str, separate_file: bool = True):
    logger_name = create_logger_name(path_file)
    if separate_file:
        log_filename = f'{logger_name}.log'
        return getLogger(name=logger_name, filename=log_filename)
    return getLogger(logger_name)