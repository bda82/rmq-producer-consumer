import asyncio
from marshmallow import Schema
from marshmallow.exceptions import ValidationError

from config import Config as FunctionConfig
from errors import *
from logger import create_logger
from utils import *


class BaseFunction:
    """Base function class"""
    
    def __init__(self, file: str = __file__, logger=None) -> None:
        self.name = self.__class__.__name__
        self.file = file if not file else __file__
        self.logger = logger or create_logger(self.file, False).getChild(self.name)
        self.config = FunctionConfig()
        self.validator = None

        self.response = {}

        self._command_pool = {}
        
    async def _validate(self, data: dict, validator: Schema) -> dict:
        try:
            validated_data = validator.dump(data)
            validated_data = validator.load(validated_data)
            return validated_data
        except ValidationError as e:
            self.logger.error(f'Validation error with Schema {validator.__class__.__name__} for {data = }')
            raise ValidationError(f'{e}')            
        
    async def _dispatch_command(self, command: str, data: dict) -> dict:
        if self.validator is None:
            raise NoValidatorDefinedError(f'No Validator defined for {self.name}')
        
        if not self._command_pool:
            raise NoCommandPoolDefinedError(f'No Command Pool defined for {self.name}')

        try:
            if data:
                validated_data = await self._validate(data, self.validator)
                self.response = await self._command_pool[command](data=validated_data)
            else:
                self.response = await self._command_pool[command](data=None)
        except ValidationError as e:
            self.logger.error(f'ValidationError in {self.name}: {e}')
            raise
        except Exception as e:
            self.logger.error(f'Unhandled exception in {self.name}: {e}')
            raise
        finally:
            return self.response
        
    async def run(self,  command: str, data: dict) -> dict:
        return await self._dispatch_command(command=command,
                                            data=data)
