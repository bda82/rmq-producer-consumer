from .base_function import BaseFunction
from logger import create_logger

from config import Config
from errors import *
from utils import *
from schemas import *
from producers import *


class MainFunction(BaseFunction):
    
    def __init__(self, file: str = ..., logger=None) -> None:
        self.name = self.__class__.__name__
        self.file = __file__
        self.logger = logger or create_logger(self.file, False).getChild(self.name)
        super().__init__(file, logger)
        self.config = Config()
        self.commands = self.config.commands
        self.validator = MainSchema()

        self._command_pool = {
            self.commands.example_command: self.__example_command,
            self.commands.loopback_message: self.__loopback_message,
        }

    async def __example_command(self, data: dict) -> dict:
        try:
            command_data = data.get('data', {})
            self.logger.info(f'Execute function {self.name} with {command_data = }')
            return self.response
        except Exception as e:
            self.logger.error(f'Command {self.name} execution error: {e}')

    async def __loopback_message(self, data: dict) -> dict:
        try:
            command_data = data.get('data', {})
            self.logger.info(f'Execute loopback function {self.name} with {command_data = }')

            await MainProducer().publish(command=self.commands.example_command,
                                         data=command_data)

            return self.response
        except Exception as e:
            self.logger.error(f'Command {self.name} execution error: {e}')
