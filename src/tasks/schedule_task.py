import asyncio

from .basic_task import BasicTask

from producers import *

from config import Config as ScheduleConfig

from logger import create_logger


class ScheduleTask(BasicTask):

    def __init__(self):
        self.file = __file__
        self.name = self.__class__.__name__
        self.config = ScheduleConfig()
        self.logger = create_logger(self.file, False).getChild(self.name)
        super().__init__(self.file, self.logger)
        self.schedule_interval = 10  # sec
        self.producer = ScheduleProducer()
        self.message = self.config.messages.schedule_message

    async def start(self):
        try:
            await asyncio.sleep(self.config.timings.schedule_start_time)
            self.logger.info(f'Start periodic task with {self.schedule_interval} sec interval')
            while True:
                await self.do_periodic()
                await asyncio.sleep(self.schedule_interval)
        except Exception as e:
            self.logger.error(f'Scheduler error: {e}')
            raise
