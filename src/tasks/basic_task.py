from abc import ABC
from utils import *
from logger import create_logger


class BasicTask(ABC):
    message = None
    producer = None
    schedule_interval = None

    def __init__(self, file: str = __file__, logger=None):
        self.name = self.__class__.__name__
        self.file = file if not file else __file__
        self.logger = logger or create_logger(self.file, False).getChild(self.name)

        if self.message is None:
            raise NotImplementedError(f'Not initialized self.message for {self.name}')
        if self.schedule_interval is None:
            raise NotImplementedError(f'Not initialized self.interval for {self.name}')
        if self.producer is None:
            raise NotImplementedError(f'Not initialized self.interval for {self.name}')

    async def do_periodic(self):
        try:
            self.logger.info(f'Start schedule task at {current_timestring()}')
            await self.producer.publish({'message': self.message})
        except Exception as e:
            self.logger.error(f'Do periodic error: {e}')
            raise

    async def start(self):
        raise NotImplementedError()
