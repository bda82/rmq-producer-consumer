from .base_producer import BaseProducer
from logger import create_logger

from config import Config as ProducerConfig

logger = create_logger(__file__, False)


class ScheduleProducer(BaseProducer):

    def __init__(self):
        super().__init__(__file__, logger)
        self.config = ProducerConfig()
        self.queue = self.config.queues.schedule_queue
        self.publisher = self.config.publishers.schedule_publisher
        self.prefix = self.config.prefixes.schedule_prefix
