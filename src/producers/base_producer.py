import json
from abc import ABC
import aiorabbit
import uvloop
from logger import create_logger

from errors import *
from utils import *

from config import Config as ProducerConfig


class BaseProducer(ABC):

    def __init__(self, file: str = __file__, logger=None):
        self.file = file if not file else __file__
        self.name = self.__class__.__name__
        self.logger = logger or create_logger(self.file, False).getChild(self.name)
        self.config = ProducerConfig()
        self.queue = None
        self.publisher = None
        self.prefix = None
        self.exchange = ''

    def __prepare(self, command: str, data: dict) -> dict:
        _uuid = generate_message_uuid()
        return {'publisher': self.publisher,
                'uuid': "".join([self.prefix, self.config.rabbit.name_separator, str(_uuid)]),
                'created': current_timestring(),
                'started': current_timestring(),
                'completed': None,
                'command': command,
                'data': data}

    async def publish(self, command: str, data: dict):
        if self.queue is None:
            raise NoQueueProvidedError(f'"queue" parameter not provided in {self.name}')

        if self.publisher is None:
            raise NoPublisherProvidedError(f'"publisher" parameter not provided in {self.name}')

        if self.prefix is None:
            raise NoPrefixProvidedError(f'"prefix" parameter not provided in {self.name}')

        try:
            publishing_message = self.__prepare(command, data)

            publishing_data = json.dumps(publishing_message)

            async with aiorabbit.connect(url=self.config.rabbit.rabbitmq_url,
                                         locale=self.config.rabbit.default_locale) as aio_client:
                await aio_client.confirm_select()

                if not await aio_client.publish(exchange=self.exchange,
                                                routing_key=self.queue,
                                                message_body=publishing_data):
                    self.logger.error(f'Publishing process failure with {publishing_message["uuid"]}')
                    raise PublishingFailureError(f'Publishing process failure with {publishing_message["uuid"]}')

            self.logger.info(f'Publish task failure with {publishing_message["uuid"]}')
        except Exception as e:
            self.logger.error(f'Publishing process failure with error: {e}')
            raise
