from .base_producer import BaseProducer
from logger import create_logger

from config import Config as ProducerConfig

logger = create_logger(__file__, False)


class MainProducer(BaseProducer):

    def __init__(self):
        super().__init__(__file__, logger)
        self.config = ProducerConfig()
        self.queue = self.config.queues.example_produce_queue
        self.publisher = self.config.publishers.example_publisher
        self.prefix = self.config.prefixes.example_prefix
